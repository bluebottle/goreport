package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

// Show JSON report
func showJSONOutput(confFile string) {
	output := generateJSONReports(confFile)
	// Display JSON output.
	fmt.Println(string(output))
} // showJSONOutput ()

// StoreJSON report.
func storeJSON(confFile string) {
	generateJSONReports(confFile)
	configuration = ReadDBConfig()
	db := pluginFunctions["NewDB"].(func(string, string, string, string) *sql.DB)
	dbCon := db(configuration.Database, configuration.Server, configuration.User, configuration.Password)
	fmt.Println(dbCon)
	// Store JSON output in database (Elasticsearch: https://es.host.funtoo.org:9200)
	// or other database backends
} // storeJSON ()

// Generate the JSON reports to be either displayed or or stored.
func generateJSONReports(confFile string) []byte {
	fmt.Println("Generating report...")
	outData.ExecutionTimers = make(map[string]float64)

	// Generate parts of output.
	for i, v := range goreport.Report {
		if i != "UUID" && v.Value == "y" {
			// Time duration of functions and add to output data.
			start := time.Now()
			tmpErr := v.funcname(&outData)
			outData.ExecutionTimers[i] = time.Since(start).Seconds() * 1000.0
			outData.ExecutionTimers["total"] += outData.ExecutionTimers[i]

			if tmpErr != "" {
				outData.FuntooReport.Errors = append(outData.FuntooReport.Errors, tmpErr)
			} // if
		} // if
	} // for

	// Add UUID, program version and timestamp to output
	outData.FuntooReport.UUID = goreport.Report["UUID"].Value
	outData.FuntooReport.Version = version
	outData.Timestamp = time.Now()
	// Combine everything in a single json file
	pciByte, _ = json.MarshalIndent(outData, "", " ")
	return pciByte
} // generateJSONReports ()

type DBConfiguration struct {
	Database string `json:"database"` // Name of the database to use.
	Server   string `json:"server"`   // Database server to connect to.
	User     string `json:"user"`     // User to connect to the server with.
	Password string `json:"password"` // Password to connect to the server with.
}

var configuration *DBConfiguration

// Read database configuration from file.
func ReadDBConfig() *DBConfiguration {
	// read in json config
	file, err := ioutil.ReadFile("dbconfig.json")

	if err != nil {
		log.Fatal(err)
	} // if

	err = json.Unmarshal(file, &configuration)

	if err != nil {
		log.Fatal(err)
	} // if

	return configuration
} // Read ()
