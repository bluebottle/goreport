package main

import (
	"flag"
	"fmt"
	"os"
	"plugin"
	"strings"
)

const (
	usage = `
    -c --config        Specify path to config file
    -u --update-config Interactively updates the config file
    -l --list-config   Lists the current configuration file's settings
    -j --show-json     Shows the JSON report
    -s --send          Sends the JSON report
    -d --debug         Enables additional debug output
    -v --verbose       Enables non-error output when sending
    -h --help          Display this help text
	-V --version       Prints the version and exits`
)

type reportPart struct {
	Value    string
	Question string
	funcname func(outData *outputData) string
}

type goReport struct {
	Report map[string]reportPart
}

var (
	configFile            = "./test.conf"
	goreport              goReport
	pluginFunctions       = make(map[string]interface{})
	availDatabases        = []string{"mariadb", "postgresql", "dgraph"}
	availDatabasesDefault = "mariadb"
)

// Initialize struct of known reports with default values.
func initializeReport() {
	goreport.Report = make(map[string]reportPart)
	goreport.Report["UUID"] = reportPart{Value: createUUID()}
	goreport.Report["boot-dir-info"] = reportPart{Value: "y", Question: "Report available kernels in /boot?", funcname: getBootDirInfo}
	goreport.Report["hardware-info"] = reportPart{Value: "y", Question: "Report information about your hardware and drivers?", funcname: getHardwareInfo}
	goreport.Report["installed-pkgs"] = reportPart{Value: "y", Question: "Report all packages installed on the system?", funcname: getAllInstalledPkg}
	goreport.Report["kernel-info"] = reportPart{Value: "y", Question: "Report information about your active kernel?", funcname: getKernelInfo}
	goreport.Report["kit-info"] = reportPart{Value: "y", Question: "Report the output of 'ego kit list'?", funcname: getKitInfo}
	goreport.Report["profile-info"] = reportPart{Value: "y", Question: "Report the output of 'epro'?", funcname: getProfileInfo}
	goreport.Report["database"] = reportPart{Value: "mariadb", Question: "Database to use (" + strings.Join(availDatabases, ",") + ")?", funcname: getProfileInfo}
} // initializeReport ()

func loadPlugin() {
	fmt.Println("Not yet")
	plug, err := plugin.Open("./plugins/" + goreport.Report["database"].Value + "/" + goreport.Report["database"].Value + ".so")

	if err != nil {
		fmt.Println(err)
	} // if

	// Check if all needed plugin functions are defined.
	for _, v := range []string{"Version", "NewDB"} {
		pluginFunctions[v], err = plug.Lookup(v)

		if err != nil {
			fmt.Println("Problem with plugin function " + v + " " + err.Error())
		} // if
	} // for
} // loadPlugin ()

func main() {
	var (
		updateConfig = false
		listConfig   = false
		showJSON     = false
		send         = false
		debug        = false
		verbose      = false
		showHelp     = false
		showVersion  = false
	)

	initializeReport()
	// Read config file.
	readConfig(configFile)
	loadPlugin()

	// Define usage function.
	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		fmt.Printf(usage + "\n")
	}
	// Define program flags.
	// Config file.
	flag.StringVar(&configFile, "c", configFile, "")
	flag.StringVar(&configFile, "config", configFile, "")
	// Update config.
	flag.BoolVar(&updateConfig, "u", false, "")
	flag.BoolVar(&updateConfig, "update-config", false, "")
	// List config.
	flag.BoolVar(&listConfig, "l", false, "")
	flag.BoolVar(&listConfig, "list-config", false, "")
	// Show JSON.
	flag.BoolVar(&showJSON, "j", false, "")
	flag.BoolVar(&showJSON, "show-json", false, "")
	// Send report.
	flag.BoolVar(&send, "s", false, "")
	flag.BoolVar(&send, "send", false, "")
	// Debug.
	flag.BoolVar(&debug, "d", false, "")
	flag.BoolVar(&debug, "debug", false, "")
	// Verbose.
	flag.BoolVar(&verbose, "v", false, "")
	flag.BoolVar(&verbose, "verbose", false, "")
	// Help.
	flag.BoolVar(&showHelp, "h", false, "")
	flag.BoolVar(&showHelp, "help", false, "")
	// Show version.
	flag.BoolVar(&showVersion, "V", false, "")
	flag.BoolVar(&showVersion, "version", false, "")
	// Parse flag definitions
	flag.Parse()

	// Show program version and exit.
	if showVersion {
		//********************************************************
		// Print program version.
		fmt.Println(os.Args[0] + " version " + version)
		//		pluginFunctions["Version"].(func())()
		pluginFunctions["Version"].(func())()
	} else if showHelp || len(os.Args) == 1 {
		//********************************************************
		// Print program usage.
		flag.Usage()
	} else if listConfig {
		//********************************************************
		// Print content of config file.
		showConfig(configFile)
	} else if showJSON {
		//********************************************************
		// Print JSON output.
		showJSONOutput(configFile)
	} else if updateConfig {
		//********************************************************
		// Update content of config file.
		updConfig(configFile)
	} else if send {
		//********************************************************
		// Store JSON into database.
		storeJSON(configFile)
	} else {
		//********************************************************
		// Print program usage.
		flag.Usage()
	} // if
} // main ()
