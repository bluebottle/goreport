# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
#PKGS:=$(shell go list -f '{{ .GoFiles }}' ./plugins/)

all: plugins build test

prod: plugins build-prod test

plugins: ./plugins/*/*.go
		for file in $^ ; do \
				$(GOBUILD) -buildmode=plugin -o $${file%go}so $${file} ; \
		done

build:
		$(GOBUILD) -v

test:
		$(GOTEST) -v

build-prod:
		$(GOBUILD) -tags prod

