package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const version = "mariadb.so 0.0.1"

// Structure of the database object.
//type DB struct {
//	server   string // server to connect to
//	user     string // user to connect with
//	password string // password to connect with
//	database string // database to connect to
//	con      string // database instance
//}

// Print current plugin version.
func Version() {
	fmt.Println(version)
} // Version ()

// Creates the database handler and checks the connection. Returns the DB pointer.
func NewDB(database, server, user, password string) *sql.DB {
	// Create the database handle, confirm driver is present
	//	db, err := sql.Open("mysql", config.User+":"+config.Password+"@unix(/var/run/mysqld/mysqld.sock)/"+config.Database)
	db, err := sql.Open("mysql", user+":"+password+"@unix(/var/run/mysqld/mysqld.sock)/"+database)

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	defer db.Close()
	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Connect and check the server version
	var version string
	err = db.QueryRow("SELECT VERSION()").Scan(&version)

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	fmt.Println("Connected to:", version)
	return db
} // NewDB ()
