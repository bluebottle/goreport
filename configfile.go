package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

// Read UUID value from /proc/sys/kernel/random/uuid
func createUUID() string {
	b, err := ioutil.ReadFile("/proc/sys/kernel/random/uuid")

	if err != nil {
		fmt.Println(err)
		return ""
	} // if

	return strings.TrimSuffix(string(b), "\n")
} // createUUID ()

// Print content of config file.
func showConfig(confFile string) {
	fmt.Println("Current configuration of " + confFile + "\n")
	b, err := ioutil.ReadFile(confFile)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(b))
	} // if
} // showConfig ()

// Read config file and return values or default values if error.
func readConfig(confFile string) {
	// Read config file content.
	b, err := ioutil.ReadFile(confFile)

	// If we could read the config file read the values in it, otherwise use standard values.
	if err == nil {
		lines := strings.Split(string(b), "\n")

		for _, v := range lines {
			// Skip comment lines, otherwise split into key - value pairs and update map.
			//			if (strings.HasPrefix(v, "#") == false) && (len(v) > 0) {
			if (!strings.HasPrefix(v, "#")) && (len(v) > 0) {
				valPair := strings.Split(v, ":")
				goreport.Report[valPair[0]] = reportPart{Value: valPair[1], Question: goreport.Report[valPair[0]].Question, funcname: goreport.Report[valPair[0]].funcname}
			} // if
		} // for
	} // if
} // readConfig

// Update content of config file.
func updConfig(confFile string) {
	readConfig(confFile)
	// Ask the user for the values.
	reader := bufio.NewReader(os.Stdin)

	for i := range goreport.Report {
		if i != "UUID" {
			if i == "database" {
				fmt.Print(goreport.Report[i].Question + " ? ")
				text, _ := reader.ReadString('\n')
				answer := strings.TrimRight(string([]byte(text)), "\n")
				goreport.Report[i] = reportPart{Value: availDatabasesDefault, Question: goreport.Report[i].Question}

				for _, v := range availDatabases {
					if answer == v {
						goreport.Report[i] = reportPart{Value: answer, Question: goreport.Report[i].Question}
					} // if
				} // for
			} else {
				fmt.Print(goreport.Report[i].Question + " yes or no? [y] ")
				text, _ := reader.ReadString('\n')
				answer := string([]byte(text)[0])

				// Check if the answer was y or n and set value, otherwise set it to y.
				if answer == "y" || answer == "n" {
					goreport.Report[i] = reportPart{Value: answer, Question: goreport.Report[i].Question, funcname: goreport.Report[i].funcname}
				} else {
					goreport.Report[i] = reportPart{Value: "y", Question: goreport.Report[i].Question, funcname: goreport.Report[i].funcname}
				} // if
			} // if
		} // if
	} // for

	// Write values to config file.
	f, err := os.Create(confFile)

	if err != nil {
		fmt.Println(err)
	} else {
		// Write header first.
		_, err := fmt.Fprintln(f, "# Generated on "+time.Now().Format("2006-01-02 15:04:05")+" for v"+version+" of GoReport")

		if err != nil {
			fmt.Println(err)
		} // if

		for i, v := range goreport.Report {
			_, err := fmt.Fprintln(f, i+":"+v.Value)

			if err != nil {
				fmt.Println(err)
			} // if
		} // for
	} // if

	err = f.Close()

	if err != nil {
		fmt.Println(err)
	} // if
} // updConfig ()
