package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type kitInfo struct {
	Name      string `json:"kit-name"`
	Branch    string `json:"branch"`
	Stability string `json:"stability"`
}

type profileInfo struct {
	Flavor       []string `json:"flavor"`
	Arch         []string `json:"arch"`
	SubArch      []string `json:"subarch"`
	PythonBranch []string `json:"python-branch"`
	MixIns       []string `json:"mix-ins"`
	Build        []string `json:"build"`
}

type installedPkgs struct {
	WorldInfo []string `json:"world-info"`
}

type pkgs struct {
	Other         map[string][]string `json:"other"`
	World         map[string][]string `json:"world"`
	PkgCountTotal uint64              `json:"pkg-count-total"`
	PkgCountWorld uint64              `json:"pkg-count-world"`
}

type cpu struct {
	Flags      []string `json:"flags"`
	Processors uint64   `json:"processors"`
	ModelName  string   `json:"model name"`
	CPUMHZ     float64  `json:"cpu MHz"`
}

type fsType struct {
	Count uint64  `json:"count"`
	Size  float64 `json:"size"`
}

type fsSys struct {
	TranTypes map[string]uint64 `json:"tran-types"`
	FsTypes   map[string]fsType `json:"fstypes"`
}

type hardwareInfo struct {
	Filesystem fsSys                        `json:"filesystem"`
	Video      map[string]map[string]string `json:"video"`
	Memory     map[string]string            `json:"memory"`
	CPU        cpu                          `json:"cpu"`
	Chassis    map[string]string            `json:"chassis"`
	Audio      map[string]map[string]string `json:"audio"`
	Networking map[string]map[string]string `json:"networking"`
}

type bootDirInfo struct {
	AvailableKernels []string `json:"available kernels"`
}

type funtooReportInfo struct {
	UUID    string   `json:"UUID"`
	Version string   `json:"version"`
	Errors  []string `json:"errors"`
}

type outputData struct {
	KernInfo        map[string]string  `json:"kernel-info"`
	KitInfo         []kitInfo          `json:"kit-info"`
	ProfInfo        profileInfo        `json:"profile-info"`
	InstPkgs        installedPkgs      `json:"installed-pkgs"`
	Pkgs            pkgs               `json:"pkgs"`
	ExecutionTimers map[string]float64 `json:"execution-timers"`
	FuntooReport    funtooReportInfo   `json:"funtoo-report"`
	HardInfo        hardwareInfo       `json:"hardware-info"`
	BootInfo        bootDirInfo        `json:"boot-dir-info"`
	Timestamp       time.Time          `json:"timestamp"`
}

var (
	pciByte []byte
	outData outputData
)

// Execute lspci -kvvmm and output result into a map. At the moment we are just grabbing video and audio from it.
func (outData *outputData) getPCI() string {
	// execute lspci -kvm
	cmd := exec.Command("lspci", "-kvvmm")
	out, err := cmd.CombinedOutput()

	if err != nil {
		return (err.Error())
	} else {
		//		var lineArr map[string]map[string]string
		//		lineArr = make(map[string]map[string]string)
		var lineArr = make(map[string]map[string]string)

		// split output string into device paragraphs and for each device split the lines
		for _, v := range strings.Split(string(out), "\n\n") {
			// If string is not empty split it into fields.
			if v != "" {
				deviceArr := strings.Split(v, "\n")
				tmpIndex := strings.SplitN(deviceArr[0], ":\t", 2)[1]
				lineArr[tmpIndex] = make(map[string]string)

				// split each line into fields, after first ":" (subject and values) and add to map
				for _, w := range deviceArr {
					if w != "" {
						tmpLine := strings.SplitN(w, ":\t", 2)
						lineArr[tmpIndex][strings.TrimSpace(tmpLine[0])] = strings.TrimSpace(tmpLine[1])
					} // if
				} // for

				var mapDest *map[string]map[string]string
				searchMap := map[string]bool{"VGA compatible controller": true, "3D controller": true, "Audio device": true}

				if _, ok := searchMap[lineArr[tmpIndex]["Class"]]; ok {
					switch lineArr[tmpIndex]["Class"] {
					case "VGA compatible controller", "3D controller":
						mapDest = &outData.HardInfo.Video
					case "Audio device":
						mapDest = &outData.HardInfo.Audio
					} // switch

					if len(*mapDest) == 0 {
						*mapDest = make(map[string]map[string]string)
					} // if

					(*mapDest)[tmpIndex] = lineArr[tmpIndex]
				} // if
			} // for
		} // if
	} // if

	return ""
} // getPCI ()

// Finding kernels in dir /boot
func getBootDirInfo(outData *outputData) string {
	//
	//
	//
	// TODO: Not always working. WHY????
	//
	//
	//
	tmpErr := ""

	for _, v := range []string{"kernel*", "vmlinuz*", "bzimage*"} {
		files, err := filepath.Glob("/boot/" + v)

		if err != nil {
			tmpErr = tmpErr + " " + err.Error()
		} // if

		if len(files) > 0 {
			// Append kernel names to info, after stripping /boot/ directory.
			for _, w := range files {
				outData.BootInfo.AvailableKernels = append(outData.BootInfo.AvailableKernels, w[6:])
			} // for
		} // if
	} // for

	return tmpErr
} // getBootDirInfo ()

// Gets networking device info from non virtual devices in /sys/class/net.
func (outData *outputData) getNet() string {
	const interfaceDir = "/sys/class/net/"
	files, err := ioutil.ReadDir(interfaceDir)

	if err != nil {
		return (err.Error())
	} // if

	for _, file := range files {
		// Skip virtual devices.
		if _, err := os.Stat(interfaceDir + file.Name() + "/device/driver/module"); !os.IsNotExist(err) {
			// Read the uevent file and get the data.
			ueventFile, err := ioutil.ReadFile(interfaceDir + file.Name() + "/device/uevent")

			if err != nil {
				return (err.Error())
			} // if

			// Create map if it doesn't exist.
			if len(outData.HardInfo.Networking) == 0 {
				outData.HardInfo.Networking = make(map[string]map[string]string)
			} // if

			outData.HardInfo.Networking[file.Name()] = make(map[string]string)

			for _, v := range strings.SplitN(string(ueventFile), "\n", -1) {
				if v != "" {
					lineContent := strings.SplitN(string(v), "=", -1)

					switch lineContent[0] {
					case "DRIVER":
						outData.HardInfo.Networking[file.Name()]["driver"] = lineContent[1]
					case "HID_ID": // For usb devices.
						idContent := strings.Split(lineContent[1], ":")
						outData.HardInfo.Networking[file.Name()]["vendor"] = strings.ToLower(strings.Trim(idContent[1], "0"))
						outData.HardInfo.Networking[file.Name()]["device"] = strings.ToLower(strings.Trim(idContent[2], "0"))
					case "PCI_ID": // For pci devices.
						idContent := strings.Split(lineContent[1], ":")
						outData.HardInfo.Networking[file.Name()]["vendor"] = strings.ToLower(idContent[0])
						outData.HardInfo.Networking[file.Name()]["device"] = strings.ToLower(idContent[1])
					} // switch
				} // if
			} // for

			// Check if it is a usb device or pci.
			devLink, err := os.Readlink(interfaceDir + "/" + file.Name())

			if err != nil {
				return (err.Error())
			} // if

			var (
				idsValues []byte
			)

			// Read the ids file and get the data.
			if strings.Contains(devLink, "usb") {
				idsValues, err = ioutil.ReadFile("/usr/share/misc/usb.ids")
			} else {
				idsValues, err = ioutil.ReadFile("/usr/share/misc/pci.ids")
			} // if

			if err != nil {
				return (err.Error())
			} // if

			// Split input to first find vendor at the beginning of line and then the first device that follows.
			tmpVendor := strings.SplitN(string(idsValues), "\n"+outData.HardInfo.Networking[file.Name()]["vendor"], -1)
			w := strings.SplitN(tmpVendor[1], "\n", -1)
			outData.HardInfo.Networking[file.Name()]["vendor"] = strings.Trim(w[0], " ")
			tmpVendor = strings.SplitN(tmpVendor[1], outData.HardInfo.Networking[file.Name()]["device"], -1)
			w = strings.SplitN(tmpVendor[1], "\n", -1)
			outData.HardInfo.Networking[file.Name()]["device"] = strings.Trim(w[0], " ")
		} // if
	} // for

	return ""
} // getNet ()

// Getting file system information.
func (outData *outputData) getFilesystemInfo() string {
	// execute lsblk --bytes -o FSTYPE,SIZE,TRAN -P
	cmd := exec.Command("lsblk", "--bytes", "-oFSTYPE,SIZE,TRAN", "-P")
	out, err := cmd.CombinedOutput()

	if err != nil {
		return ("Error lsblk command: " + err.Error() + " " + string(out))
	} // if

	outArr := strings.Split(string(out), "\n")

	var (
		tranCount map[string]uint64
		fsCount   map[string]uint64
		fsSize    map[string]uint64
	)

	tranCount = make(map[string]uint64)
	fsSize = make(map[string]uint64)
	fsCount = make(map[string]uint64)

	for _, v := range outArr {
		if v != "" {
			//	Removing "" around values
			lineArr := strings.Split(strings.Replace(v, "\"", "", -1), " ")
			// Splitting up the three fields.
			fstypePair := strings.Split(lineArr[0], "=")
			sizePair := strings.Split(lineArr[1], "=")
			tranPair := strings.Split(lineArr[2], "=")

			if fstypePair[1] == "" && tranPair[1] != "" {
				if tranPair[1] != "" {
					tranCount[tranPair[1]]++
				} else {
					tranCount[tranPair[1]]++
				} // if
			} else if fstypePair[1] == "" && tranPair[1] == "" {
				tmpVal, err := strconv.ParseInt(sizePair[1], 10, 64)

				if err != nil {
					return "Error converting: " + err.Error()
				} // if

				fsSize["unreported"] += (uint64)(tmpVal)
				fsCount["unreported"]++
			} else {
				tmpVal, err := strconv.ParseInt(sizePair[1], 10, 64)

				if err != nil {
					return "Error converting: " + err.Error()
				} // if

				fsSize[fstypePair[1]] += (uint64)(tmpVal)
				fsCount[fstypePair[1]]++
			} // if
		} // if
	} // for

	outData.HardInfo.Filesystem.TranTypes = make(map[string]uint64)
	outData.HardInfo.Filesystem.FsTypes = make(map[string]fsType)

	for i, v := range tranCount {
		outData.HardInfo.Filesystem.TranTypes[i] = v
	} // for

	for i, v := range fsSize {
		outData.HardInfo.Filesystem.FsTypes[i] = fsType{Count: fsCount[i], Size: math.Round((float64(v)/math.Pow(1024, 3))*100) / 100}
	} // for

	return ""
} // getFilesystemInfo ()

// Getting cpu information from /proc/cpuinfo.
func (outData *outputData) getCPU() string {
	// Read the cpuinfo file and get the data.
	cpuFile, err := ioutil.ReadFile("/proc/cpuinfo")

	if err != nil {
		return err.Error()
	} // if

	for _, v := range strings.SplitN(string(cpuFile), "\n", -1) {
		if v != "" {
			fieldArr := strings.Split(v, ":")

			switch w := strings.TrimSpace(fieldArr[0]); w {
			case "model name": // Get cpu model.
				outData.HardInfo.CPU.ModelName = fieldArr[1]
			case "flags": // Get available use flags.
				outData.HardInfo.CPU.Flags = strings.Split(strings.TrimSpace(fieldArr[1]), " ")
			case "cpu MHz": // Get current cpu speed. Could vary depending on the used govenor and system usage.
				tmpFloat, err := strconv.ParseFloat(strings.TrimSpace(fieldArr[1]), 64)

				if err != nil {
					return err.Error()
				} // if

				outData.HardInfo.CPU.CPUMHZ = tmpFloat
			case "processor": // Count number of available cores.
				outData.HardInfo.CPU.Processors++
			} // switch
		} // if
	} // for

	return ""
} // getFilesystemInfo ()

// Getting chassis information from /sys/class/dmi/id/ files.
func (outData *outputData) getChassis() string {
	// Slice of possible results for chassis type.
	typeArr := []string{"N/A", "Other", "Unknown", "Desktop", "Low Profile Desktop", "Pizza Box", "Mini Tower", "Tower", "Portable", "Laptop",
		"Notebook", "Hand Held", "Docking Station", "All in One", "Sub Notebook", "Space-Saving", "Lunch Box", "Main Server Chassis", "Expansion Chassis",
		"SubChassis", "Bus Expansion Chassis", "Peripheral Chassis", "RAID Chassis", "Rack Mount Chassis", "Sealed-Case PC", "Multi-system Chassis",
		"Compact PCI", "Advanced TCA", "Blade", "Blade Enclosure", "Tablet", "Convertible", "Detachable", "IoT Gateway", "Embedded PC", "Mini PC", "Stick PC"}

	// Read the chassis type file.
	tmpFile, err := ioutil.ReadFile("/sys/class/dmi/id/chassis_type")

	if err != nil {
		return err.Error()
	} // if

	tmpInt, _ := strconv.ParseInt(strings.TrimRight(string(tmpFile), "\n"), 10, 64)
	outData.HardInfo.Chassis = make(map[string]string)
	outData.HardInfo.Chassis["chassis_type"] = typeArr[tmpInt]

	// Read the chassis vendor file.
	tmpFile, err = ioutil.ReadFile("/sys/class/dmi/id/chassis_vendor")

	if err != nil {
		return err.Error()
	} // if

	outData.HardInfo.Chassis["chassis_vendor"] = strings.TrimRight(string(tmpFile), "\n")

	// Read the product name file.
	tmpFile, err = ioutil.ReadFile("/sys/class/dmi/id/product_name")

	if err != nil {
		return err.Error()
	} // if

	outData.HardInfo.Chassis["product_name"] = strings.TrimRight(string(tmpFile), "\n")
	return ""
} // getChassis ()

// Getting memory information.
func (outData *outputData) getMem() string {
	// Read the meminfo file and get the data.
	memFile, err := ioutil.ReadFile("/proc/meminfo")

	if err != nil {
		return err.Error()
	} // if

	outData.HardInfo.Memory = make(map[string]string)

	for _, v := range strings.Split(string(memFile), "\n") {
		if v != "" {
			fieldArr := strings.Split(v, ":")
			w := strings.TrimSpace(fieldArr[0])

			switch w {
			case "MemAvailable", "MemTotal", "SwapFree", "SwapTotal", "MemFree":
				tmpVar, err := strconv.ParseUint((strings.Split(strings.TrimSpace(fieldArr[1]), " "))[0], 10, 64)

				if err != nil {
					return err.Error()
				} // if

				outData.HardInfo.Memory[w] = fmt.Sprintf("%.2f", float64(tmpVar)/1024/1024)
			} // switch
		} // if
	} // for

	return ""
} // getMem ()

func getHardwareInfo(outData *outputData) string {
	return outData.getPCI() + " " + outData.getNet() + " " + outData.getFilesystemInfo() + " " +
		outData.getCPU() + " " + outData.getMem() + " " + outData.getChassis()
} // getHardwareInfo ()

// Getting the full list of installed packages.
func getAllInstalledPkg(outData *outputData) string {
	const (
		worldFileName  = "/var/lib/portage/world"
		packageDirName = "/var/db/pkg"
	)

	// Read all installed packages.
	mainDirs, err := ioutil.ReadDir(packageDirName)

	if err != nil {
		return err.Error()
	} // if

	var instPkgs = make(map[string][]string)

	// Create slice of all installed packages.
	for _, file := range mainDirs {
		// Skip files as we only want directories on this level.
		if file.IsDir() {
			subDirs, err := ioutil.ReadDir(packageDirName + "/" + file.Name())

			if err != nil {
				return err.Error()
			} // if

			for _, subFile := range subDirs {
				// Skip half emerged files.
				if !strings.Contains(subFile.Name(), "-MERGING-") {
					// Split version from name.
					tmpInd := strings.Index(subFile.Name(), ".")
					var tmpInd2 int

					if tmpInd > -1 {
						tmpInd2 = strings.LastIndex(subFile.Name()[:tmpInd], "-")
					} else {
						tmpInd2 = strings.LastIndex(subFile.Name(), "-")
					} // if

					instPkgs[file.Name()+"/"+subFile.Name()[:tmpInd2]] = append(instPkgs[file.Name()+"/"+subFile.Name()[:tmpInd2]], subFile.Name()[tmpInd2+1:])
				} // if
			} // for
		} // if
	} // for

	// Read world file.
	worldFile, err := ioutil.ReadFile(worldFileName)

	if err != nil {
		return err.Error()
	} // if

	outData.Pkgs.World = make(map[string][]string)

	for _, pack := range strings.Split(string(worldFile), "\n") {
		if len(instPkgs[pack]) > 0 {
			// Add all versions to world info and pkgs/world.
			for _, v := range instPkgs[pack] {
				outData.InstPkgs.WorldInfo = append(outData.InstPkgs.WorldInfo, pack+"-"+v)
				outData.Pkgs.World[pack] = append(outData.Pkgs.World[pack], v)
			} // for

			// Remove processed world element from all packages.
			delete(instPkgs, pack)
		} // if
	} // for

	// Add rest of packages to pkgs/other.
	outData.Pkgs.Other = make(map[string][]string)

	for i, v := range instPkgs {
		outData.Pkgs.Other[i] = append(outData.Pkgs.Other[i], v...)
		//		for _, w := range v {
		//			outData.Pkgs.Other[i] = append(outData.Pkgs.Other[i], w)
		//		} // for
	} // for

	// Add counts.
	outData.Pkgs.PkgCountWorld = uint64(len(outData.Pkgs.World))
	outData.Pkgs.PkgCountTotal = outData.Pkgs.PkgCountWorld + uint64(len(outData.Pkgs.Other))
	return ""
} // getAllInstalledPkg ()

// Fetching kernel information from /proc/sys/kernel files.
func getKernelInfo(outData *outputData) string {
	outData.KernInfo = make(map[string]string)

	for _, v := range []string{"osrelease", "ostype", "version"} {
		tmpFile, err := ioutil.ReadFile("/proc/sys/kernel/" + v)

		if err != nil {
			return err.Error()
		} // if

		outData.KernInfo[v] = strings.TrimSuffix(string(tmpFile), "\n")
	} // for

	return ""
} // getKernelInfo ()

// Fetching active kits.
func getKitInfo(outData *outputData) string {
	// execute ego kit list
	cmd := exec.Command("ego", "kit", "list")
	out, err := cmd.CombinedOutput()

	if err != nil {
		return err.Error()
	} else {
		var (
			tmpCols []string
			tmpName string
		)

		// The lines that we want should be the ones with "kit" in the name and be "active".
		for _, v := range strings.Split(string(out), "\n") {
			// Filter out lines which are too short.
			if len(v) > 55 {
				tmpCols = []string{strings.Trim(v[:20], " "), strings.Trim(v[21:37], " "), strings.Trim(v[38:53], " "), strings.Trim(v[54:], " ")}

				// Store kit name for kits with multiple branches.
				if tmpCols[0] != "" {
					tmpName = tmpCols[0]
				} // if

				// Act on active kit branches.
				if strings.Contains(tmpCols[1], "active") && !strings.Contains(v, "stability") {
					outData.KitInfo = append(outData.KitInfo, kitInfo{tmpName, tmpCols[2], tmpCols[3]})
				} // if
			} // if
		} // for
	} // if

	return ""
} // getKitInfo ()

// Extract profile info from epro, including inherited elements.
func getProfileInfo(outData *outputData) string {
	// execute ego kit list
	cmd := exec.Command("epro")
	out, err := cmd.CombinedOutput()

	if err != nil {
		return err.Error()
	} else {
		var outputDest string

		// Parse command output and switch resulting output according to parsed input lines.
		for _, v := range strings.Split(string(out), "\n") {
			switch {
			//case strings.Index(v, "arch:") > 0 && strings.Index(v, "subarch:") == -1:
			case strings.Index(v, "arch:") > 0 && !strings.Contains(v, "subarch:"):
				outData.ProfInfo.Arch = append(outData.ProfInfo.Arch, strings.Trim(strings.SplitAfter(v, "arch:")[1], " "))
			case strings.Index(v, "subarch:") > 0:
				outData.ProfInfo.SubArch = append(outData.ProfInfo.SubArch, strings.Trim(strings.SplitAfter(v, "subarch:")[1], " "))
			case strings.Index(v, "build:") > 0:
				outData.ProfInfo.Build = append(outData.ProfInfo.Build, strings.Trim(strings.SplitAfter(v, "build:")[1], " "))
			case strings.Index(v, "  flavor:") > 0:
				outData.ProfInfo.Flavor = append(outData.ProfInfo.Flavor, strings.Trim(strings.SplitAfter(v, "flavor:")[1], " "))
			case strings.Index(v, "Python kit:") > 0:
				outputDest = "python-branch"
			case strings.Index(v, "All inherited flavor from hardened flavor:") > 0:
				outputDest = "flavor"
			case strings.Index(v, "  mix-ins:") > 0:
				outData.ProfInfo.MixIns = append(outData.ProfInfo.MixIns, strings.Trim(strings.SplitAfter(v, "mix-ins:")[1], " "))
				outputDest = "mixin"
			case strings.Index(v, "All inherited mix-ins from hardened flavor:") > 0, strings.Index(v, "All inherited mix-ins from media mix-ins:") > 0:
				outputDest = "mixin"
			default:
				if v != "" {
					switch outputDest {
					case "python-branch":
						if strings.Index(v, "branch:") > 0 {
							outData.ProfInfo.PythonBranch = append(outData.ProfInfo.PythonBranch, strings.Trim(strings.SplitAfter(v, "branch:")[1], " "))
						} // if
					case "flavor":
						if strings.Index(v, "(from ") > 0 {
							outData.ProfInfo.Flavor = append(outData.ProfInfo.Flavor, strings.Trim(strings.Split(v, "(from ")[0], " "))
						} // if
					case "mixin":
						if strings.Index(v, "(from ") > 0 {
							outData.ProfInfo.MixIns = append(outData.ProfInfo.MixIns, strings.Trim(strings.Split(v, "(from ")[0], " "))
						} // if
					} // switch
				} // if
			} // switch
		} // for
	} // if

	return ""
} // getProfileInfo ()
